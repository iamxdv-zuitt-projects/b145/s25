
//Find Operation

// db.collections.find({query}, {field projection})


//Query Operators

    /*Comparison Query Operators*/
        // Syntax: { field: { $lt: value } }

    //mini activity
        //look for a document/s that has an age of less than 50
        db.users.find(
            {
                "age": { $lt: 50}
            }
        );
        //returned 1 document


        //look for a document/s that has an age of greater than or equal to 50
        db.users.find(
            {
                "age": { $gte: 50}
            }
        );
        //returned 3 document


        //look for a document/s that has an age of not equal to 82
        db.users.find({
            "age": { $ne: 82 }
        });
        //returned 3 document
        

        //look for a document/s that has names hawking and doe
        db.users.find(
            {
                "lastName": { $in: ["Hawking", "Doe"] }
            }
        );
        //returned 2 documents


        //look for a document/s that has courses HTML and React
        db.users.find(
            {
                "courses": { $in: ["HTML", "React"] }
            }
        );
        //returned 3 documents


    /*Logical Query Operators*/

        //"OR" logical operator

        //look for a document/s that has a name Neil or has an age of 25
        db.users.find(
            {
                $or: [ 
                    {"firstName": "Neil"}, 
                    {"age": 25}
                ]
            }
        );

        //look for a document/s that has a name Neil or has an age of greater than 30
        db.users.find(
            {
                $or: [ 
                    {"firstName": "Neil"}, 
                    {"age": {$gt: 30}}
                ]
            }
        );
        //returned 3 documents




        //"AND" logical operator
        
        //look for a document/s that has an age of not equal to 82 and an age not equal to 76
        db.users.find(
            {
                $and: [
                    {"age": {$ne: 82} },
                    {"age": {$ne: 76} }
                ]
            }
        );


//Field Projection
    // allows us to include or exclude specific fields in a returning document
    /*The projection parameter determines which fields are returned in the matching documents.*/

    db.users.find(
        {
            "firstName": "Jane"
        },
        {
            "firstName": 1,
            "lastName": 1,
            "contact": 1
        }
    );


    //look for a document/s that has a name hawking and exclude contact and department fields in the returning document
    db.users.find(
        {
            "lastName": "Hawking"
        },
        {
            "contact": 0,
            "department": 0
        }
    );

    //Suppresing the ID field
        //look for a document/s that has a name Neil and exclude id but include first & last name and contact fields in the returning document
        db.users.find(
            {
                "firstName": "Neil"
            },
            {
                "_id": 0,
                "firstName": 1,
                "lastName": 1,
                "contact": 1
            }
        );


    //look for a document/s that has a name Bill and include first & last name and phone number fields in the returning document
    db.users.find(
        {
            "firstName":"Bill"
        },
        {
            "firstName": 1,
            "lastName": 1,
            "contact.phone": 1,
            "_id": 0
        }
    );


    //Suppressing Specific Fields in embedded documents
        //look for a document/s that has a name Bill and include everything else except email field
    db.users.find(
        {
            "firstName":"Bill"
        },
        {
            "_id": 1,
            "firstName": 1,
            "lastName": 1,
            "age": 1,
            "contact.phone": 1,
            "courses": 1,
            "department": 1
        }
    );

    //another solution:
    db.users.find(
        {
            "firstName":"Bill"
        },
        {	
            "contact.email": 0,	
        }
    );




    /*Evaluation Query Operator*/

        //$regex operator
        db.users.find(
            {
                "firstName": { $regex: "N" }
            }
        );
        //returned only one document due to case sensitivity

        db.users.find(
            {
                "firstName": { $regex: "n" }
            }
        );
        //returned 2 documents due to case sensitivity


        //Solution: add $options to avoid case sensitivity
        db.users.find(
            {
                "firstName": { $regex: "n", $options: "i" }
            }
        );
        //returned 3 documents

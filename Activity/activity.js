// Activity
    // Use MongoDB Query Operators and Field Projection to retrieve documents from our database

// 1. Create an activity.js file on where to write and save the solution for the activity


//2. Find users with letter "s" in their first name or "d" in their last name.
    // a. Use the $or operator
    // b. Show only the firstName and lastName fields and hide the _id field.
    
    // Answer
    db.users.find(
        {
            $or: [ 
                {"firstName": { $regex: "s", $options: "i" }}, 
                {"lastName": { $regex: "d", $options: "i" }}
            ]
        },
        {
            "_id": 0,
            "age": 0,
            "contact.phone": 0,
            "courses": 0,
            "department": 0
        }
    );

// 3. Find users who are from the "HR department" and their age is greater then or equal to 70
    // a. Use the $and operator
        // Answer:
        db.users.find(
            {
                $and: [
                    {"department": "HR" },
                    {"age": {$gte: 70} }
                ]
            }
        );


// 4.Find users with the letter "e" in their firstName and has an age of less than or equal to 30.
    // a. Use the $and $regex and Site operators

    // Answer:
    db.users.find(
        {
            $and: [
                {"firstName": { $regex: "e", $options: "i" }},
                {"age": {$lte: 30} }
            ]
        }
    );


//5. Create a git repository named s26


// 6. Initialize a local git repository, add the remote link and push to git with the commit message of "Add activity code"


// 7. Add the link in Boodle under "WD078-24 | MongoDB - Query Operators and Field Projection" section topic.

